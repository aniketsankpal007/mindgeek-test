package TestSuite;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class VerifyLatestMenuTest {

	@Test
	public void verifyLatest() throws InterruptedException
	{
		
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		WebDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.theweathernetwork.com/ca");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(5000);
		driver.findElement(By.linkText("See All News")).click();
	
	
				
		Actions action = new Actions(driver);
		WebElement latestLink = driver.findElement(By.linkText("Latest"));
		String blueBorderColor="3px solid rgb(67, 154, 211)";
		String hoverValue = latestLink.getCssValue("border-bottom");
	//	action.click(latestLink);
		
		if(hoverValue.equals(blueBorderColor))
		{
			System.out.println("Before clicking on Latest, Latest Menu has been underlined");
		}
		else
		{
			System.out.println("Latest Menu has not been underlined");
		}
			
		driver.close();
	
	}

}

